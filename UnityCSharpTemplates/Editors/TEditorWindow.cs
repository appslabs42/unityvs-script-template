﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class TEditorWindow : EditorWindow
{
    private static TEditorWindow window;

    [MenuItem("MyWindow/Show Window")]
    private static void Init()
    {
        window = EditorWindow.GetWindow<TEditorWindow>();
        window.Show();
    }
}
