﻿using UnityEngine;
using System.Collections;

public class MonoSingleton : MonoBehaviour
{
    #region Singleton Delcaration
    public static MonoSingleton singleton { get { return m_Singleton; } }
    private static MonoSingleton m_Singleton;

    //Default constructor
    private MonoSingleton() { m_Singleton = this; }
    #endregion

}
