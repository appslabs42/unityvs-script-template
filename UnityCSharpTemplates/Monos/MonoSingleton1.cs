﻿using UnityEngine;
using System.Collections;

public class MonoSingleton1 : MonoBehaviour
{
    #region Singleton-Monobehaviour.Awake()
    public static MonoSingleton1 instance = null;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    #endregion
}
