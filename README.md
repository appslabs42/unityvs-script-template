# Unity3D-Visual Studio C# Script Templates#

MonoBehaviour and UnityEditor script templates for visual studio.

### Downloads ###
* [UnityTemplate.rar](https://bitbucket.org/iamvishnusankar/unityvs-script-template/raw/3e281f99a1318c831ae324bcbe355e5a524beccc/Downloads/UnityTemplate.rar)

### How to install? ###

1. Extract **"UnityTemplate.rar"** and Copy the **“Unity C# Templates”** folder to **“/Documents/Visual Studio** *Version* **/Templates/ItemTemplates/** ”. ( [How to locate VS Template folder](https://msdn.microsoft.com/en-us/library/y3kkate1.aspx#Anchor_0))
2. Restart visual studio.
3. Enjoy

####Editor Templates
* Custom Editor Window
* Custom Inspector
* Scriptable Object


####Monobehaviour Templates
* Empty Static class
* Mono Awake
* Mono Empty
* Mono Singleton
* Mono Start, Update
* Mono Start